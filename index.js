const express = require ('express');    //  Importe le module express
const app = express();  //  Appelle express
const bodyParser = require ('body-parser'); //  Importe le module body-parser
const router = require ('./routes/route');
const mysql = require('mysql2'); // Importe MySQL 
const passport = require('passport'); // Importe passport
const session = require('express-session'); // Importe express-session
port = process.env.PORT || 4000;    //  Définit le port 


//*Connexion à la BDD
const conn = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "sampleDB"
});

// PASSPORT CONFIG
require('./middleware/passportAuth')(passport);

app.set('view engine', 'pug');  //  Définit le moteur de view
app.use(bodyParser.json()); //Recupere sous forme de json
app.use(express.urlencoded({ extended : false }));  // Définit un encodage classique via bodyparser
app.use(express.static(__dirname + '/public')); //  Définit le début de la route pour les fichiers statiques

// Express session
app.use(
    session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
    })
);
// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.use('/', router);
// Test
app.listen(port, function(){
    console.log('OKIDOKI!');
})
conn.connect(function(err) {
    if (err) throw err;
    console.log("BDD Connected!");
});