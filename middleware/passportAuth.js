const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const conn = require('../config/dbconnection')

module.exports = function(passport) {
    passport.use('local',
        new LocalStrategy({usernameField: 'pseudo_user', passwordField: 'password_user'}, (pseudo_user, password_user, done) => {
            if(!pseudo_user || !password_user){
                return done(null, false, req.flash('message','Tous les champs sont requis')); 
            }
            conn.query('SELECT pseudo_user, password_user, role_user FROM user WHERE pseudo_user= "'+ pseudo_user +'"', function (error, results, fields) {
                if (error) throw error;
                if (!results){
                    return done(null, false, {message : 'Cet utilisateur n\'est pas enregistré'}); // VERIFICATION DU MOT DE PASSE
                }
                bcrypt.compare(password_user, results[0].password_user, (err, isMatch) => {
                    if (err) throw err;
                    if (isMatch) {
                        return done(null, results[0]);
                    } else {
                        return done(null, false, {message:'Mot de passe incorrect'}); 
                    }
                });
            });
        })
    )

    passport.serializeUser(function(user, done) {
        console.log("serialize : ", user)
        done(null, user);

    });

    passport.deserializeUser(function(user, done){
        conn.query('SELECT pseudo_user, role_user, password_user, email_user FROM user WHERE pseudo_user = "'+ user.pseudo_user +'"', function (err, results){   
            done(err, results[0]);
        });
    });
}