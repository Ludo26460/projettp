module.exports = function isAdmin(req, res, next) {
    let role_user = req.user.role_user
    
    if(role_user !== 0){
        console.log('Vous ne passerez pas!')
        res.redirect('/accueil');
    }else{
        next();
    }
    
}