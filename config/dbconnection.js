const mysql = require('mysql2'); // Appelle MySQL
const conn = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "raisedb",
    multipleStatements: true
});

module.exports = conn;