const express = require ('express');
const passport = require('passport'); 
const bcrypt = require('bcryptjs');
const conn = require('../config/dbconnection')
const isAdmin = require('../middleware/isAdmin')


function quoteEscape(str){
    return (str+'').replace(/([\\"'])/g, "\\$1").replace(/\0/g, "\\0");  
}

const router = express.Router();

router.get('/', function(req, res){
    console.log(req.session)
    res.render('connexion')
});
router.post('/', (req, res, next) => {
    let {pseudo_user, password_user} = req.body
    console.log(pseudo_user, password_user)
    passport.authenticate('local', {
        successRedirect : '/accueil',
        failureRedirect : '/',
        failureFlash : true
    })
    (req, res, next);
})
router.get('/deconnexion', function(req, res){
    req.logout();
    res.redirect('/');
})

router.get('/inscription', function(req, res){
    res.render('inscription')
});
router.post('/inscription', function(req,res){
    let nom_rgpd = req.body.nom_rgpd
    let prenom_rgpd = req.body.prenom_rgpd
    let pseudo_user = req.body.pseudo_user
    let email_user = req.body.email_user
    let password_user = req.body.password_user
    let id_user = 0
    // let infosRgpdQuery = 'INSERT INTO rgpd (nom_rgpd, prenom_rgpd, id_user) VALUES ("' + nom_rgpd + '", "' + prenom_rgpd + '", "' + id_user + '")'
        
    //TODO : Tester les inputs avant de lancer la requête 
    bcrypt.genSalt(10, (err, salt) => {
        if (err){
            throw err;
        } 
        bcrypt.hash(password_user, salt, (err, hash) => {        
            if (err) throw err;                    
            conn.query('INSERT INTO user (pseudo_user, email_user, password_user) VALUES ("' + pseudo_user + '", "' + email_user +'", "' + hash +'")', function (err, results, fileds) {
                if (err){
                    throw err;
                }else{
                    console.log(results)
                    conn.query('SELECT id_user FROM `user` WHERE pseudo_user= "' + pseudo_user + '"', function (err, results){
                        if (err){
                            throw err;
                        }                  
                        else{
                            id_user = results[0].id_user;
                            console.log(id_user)
                            if( id_user != 0 ){
                                conn.query('INSERT INTO rgpd (nom_rgpd, prenom_rgpd, id_user) VALUES ("' + nom_rgpd + '", "' + prenom_rgpd + '", "' + id_user + '")', function(err, results){
                                    if (err){
                                        throw err;
                                    }
                                    else{
                                        console.log('All is Ok!')
                                        res.redirect('/');
                                    }
                                })
                            }
                        }
                    });
                }
            });
        });
    });
});

router.get('/oublimdp', function(req, res){
    res.render('oublimdp')
});

router.get('/accueil', function(req, res){
    res.render('accueil', {userName : req.user.pseudo_user})
});

router.get('/profil', function(req, res){
    res.render('profil', {eMailUser : req.user.email_user, userName : req.user.pseudo_user})
});

router.get('/emailmodifier', function(req, res){
    res.render('emailmodifier', {eMailUser : req.user.email_user, userName: req.user.pseudo_user})
})
router.post('/emailmodifier', function(req, res){
    let pwCheck = req.body.pwCheck
    let emailChange = req.body.emailChange
    let emailChangeConfirmation = req.body.emailChangeConfirmation
    let pseudo_user= req.user.pseudo_user
    let emailModQuery = "UPDATE user SET email_user = '" + emailChange + "' WHERE pseudo_user = '" + pseudo_user + "' "
    
    bcrypt.compare(pwCheck, req.user.password_user, (err, isMatch) => {
        if (err){
            throw err;
        }
        if (isMatch && emailChange == emailChangeConfirmation){
            conn.query(emailModQuery, function(err, results){
                if (err){
                    throw err;
                }
                else{
                    console.log('Votre email a été changé')
                    res.redirect('/profil')
                }
            })
        }
        else{
            console.log('Password et/ou confirmation incorrect!')
            res.redirect('/profil')
        }
    });
})

router.get('/mdpmodifier', function(req, res){
    res.render('mdpmodifier', {userName: req.user.pseudo_user})
})
router.post('/mdpmodifier', function(req, res){
    let pwCheck = req.body.pwCheck
    let pwChange = req.body.pwChange
    let pwChangeConfirmation = req.body.pwChangeConfirmation
    let pseudo_user= req.user.pseudo_user

    bcrypt.compare(pwCheck, req.user.password_user, (err, isMatch) => {
        if (err){
            throw err;
        }
        if (isMatch && pwChange == pwChangeConfirmation){
            bcrypt.genSalt(10, (err, salt) => {
                if (err) throw err;
                bcrypt.hash(pwChange, salt, (err, hash) => {        
                    if (err) throw err;                    
                    conn.query("UPDATE user SET password_user = '" + hash + "' WHERE pseudo_user = '" + pseudo_user  + "'", function (err, results, fields) {
                        if (err){
                            throw err;
                        }
                        else{
                            console.log('Mot de passe changé!')
                            res.redirect('/profil')
                        } 
                    });
                });
            });
        }
        else{
            console.log('Password et/ou confirmation incorrect!')
            res.redirect('/profil')
        }
    });
})

router.post('/marque', function(req, res){
    res.redirect('/marque/' + req.body.searchMarque); // Recupere l'input du form via methode post puis redirige vers /marque/req.body.[input.name]
})
router.get('/marque/:nom_marque', function(req, res){
    let nom_marque = req.params.nom_marque;
    console.log(nom_marque);
    let marqueQuery = "SELECT * FROM `marque` JOIN `lobby` ON lobby_id_lobby = id_lobby WHERE nom_marque = '" + nom_marque + "'"

    conn.query(marqueQuery, function(err, results){
        if(err){
            throw err;
        }
        else{
            console.log(results)
            res.render('marque', {marqueInfos: results[0], userName: req.user.pseudo_user});
        }
    }) 
});

router.get('/marquemodifier/:nom_marque', isAdmin, function(req, res){
    let nom_marque = req.params.nom_marque;
    console.log(nom_marque);
    let marque_lobbyQuery = "SELECT * FROM `marque` JOIN `lobby` ON lobby_id_lobby = id_lobby WHERE nom_marque = '" + nom_marque + "'; SELECT nom_lobby FROM `lobby`"

    conn.query(marque_lobbyQuery, function(err, results){
        if(err){
            throw err;
        }
        else{
            console.log(results)
            res.render('marquemodifier', {marque: results[0][0], lobbyInfos: results[1], userName: req.user.pseudo_user});
        }
    })
});

router.post('/marquemodifier/:nom_marque', function(req, res){
    let desc_marque = quoteEscape(req.body.desc_marque)
    let nom_marque = quoteEscape(req.body.nom_marque)
    let lobby = req.body.lobby
    let id_lobby = 0
    
    conn.query('SELECT id_lobby FROM `lobby` WHERE nom_lobby = "' + lobby + '"', function(err, results){
        if (err){
            throw err;
        }
        else{
            let id_lobby = results[0].id_lobby
            let updateMarqueQuery = "UPDATE marque SET nom_marque = '" + nom_marque + "', pastille_marque = '" + req.body.grade + "', desc_marque='" + desc_marque + "', lobby_id_lobby = '" + id_lobby + "' WHERE nom_marque = '" + req.params.nom_marque + "'"
            conn.query(updateMarqueQuery, function(err, results){
                console.log(err); 
                if(err==null){
                    console.log('All is Ok!')
                }
            res.redirect('/marquemodifier/' +req.body.nom_marque)
            });
        }
    })
})

router.get('/lobbymodifier/:nom_lobby', isAdmin, function(req, res){
    let nom_lobby = req.params.nom_lobby
    console.log(nom_lobby);
    let lobbyQuery = "SELECT * FROM `lobby` WHERE nom_lobby = '" + nom_lobby + "'"
    conn.query(lobbyQuery, function(err, results){
        if(err){
            throw err;
        }
        else{
            console.log(results); 
            res.render('lobbymodifier', {Lobby : results[0], userName: req.user.pseudo_user});
        }
    });
});
router.post('/lobbymodifier/:nom_lobby', function(req, res){
    let nom_lobby = quoteEscape(req.body.nom_lobby)
    let desc_lobby = quoteEscape(req.body.desc_lobby)
    let updateLobbyQuery = "UPDATE lobby SET nom_lobby = '" + nom_lobby + "', pastille_lobby = '" + req.body.grade + "', desc_lobby = '" + desc_lobby +"' WHERE nom_lobby = '" + req.params.nom_lobby +"'"
    conn.query(updateLobbyQuery, function(err, results){
        console.log(err, results)
        if (err){
            throw err;
        }
        else{
            res.redirect('/lobbymodifier/' + req.body.nom_lobby)
        }
    })
})

router.get('/creationmarque', isAdmin, function(req, res){
    conn.query(
        'SELECT nom_lobby FROM `lobby`',
        function(err, results) {
            // console.log(results); 
            res.render('creationmarque', {listeLobby: results, userName: req.user.pseudo_user})
        });
});
router.post('/creationmarque', function(req, res){
    let couleurPastille = req.body.grade
    let nom_marque = quoteEscape(req.body.nom_marque)
    let desc_marque = quoteEscape(req.body.desc_marque)
    let lobby = req.body.nom_lobby
    let id_lobby = 0
    
    conn.query('SELECT id_lobby FROM lobby WHERE nom_lobby= "' + lobby + '"', function(err, results){
        if(err){
            throw err;
        }
        else{
            console.log(results)
            let id_lobby = results[0].id_lobby
            console.log(id_lobby)
            let creationMarqueQuery = 'INSERT INTO marque (nom_marque, pastille_marque, desc_marque, lobby_id_lobby) VALUES ("' + nom_marque +'", "' + couleurPastille +'", "' + desc_marque +'", "' + id_lobby +'")'
            conn.query(creationMarqueQuery, function(err, results){
                if(err==null){
                    console.log('All is Ok!')
                }
            res.redirect('/marque/'+nom_marque)    
            });
        }
    })
})

router.get('/creationlobby', isAdmin, function(req, res){
    res.render('creationlobby', {userName: req.user.pseudo_user})
});
router.post('/creationlobby', function(req, res){
    let couleurPastille = req.body.grade
    let nom_lobby = quoteEscape(req.body.nom_lobby)
    let desc_lobby = quoteEscape(req.body.desc_lobby)
    let creationLobbyQuery = 'INSERT INTO lobby (nom_lobby, pastille_lobby, desc_lobby) VALUES ("' + nom_lobby + '", "' + couleurPastille + '", + "' + desc_lobby + '")'

    conn.query(creationLobbyQuery, function(err, results){
            if(err==null){
                console.log('OK')
            }
        });
    res.redirect('/creationlobby')
})

router.get('/destroy', function(req, res){
    console.log(req.user.password_user + ' routegetdestroy')
    res.render('destroy', {userName: req.user.pseudo_user, userPw: req.user.password_user});
})
router.post('/destroy', function(req, res){
    let pwDestroy = req.body.pwDestroy;
    let pwDestroyConfirmation = req.body.pwDestroyConfirmation;
    let deleteUserQuery = "DELETE FROM `user` WHERE pseudo_user='" + req.user.pseudo_user + "' "
    let userPw = req.user.password_user

    if(pwDestroy != pwDestroyConfirmation){
        res.redirect('/profil');
    }else{
        bcrypt.compare(pwDestroy, userPw, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
                conn.query(deleteUserQuery, function(err, results){
                    if(err==null){
                        console.log("Supprimé")
                    }
                })
                req.logout();
                res.redirect('/');
            }
            else{
                console.log('WRONG PW')
                res.redirect('/profil');
            }
        });
        console.log(req.user.pseudo_user)
    }
})

module.exports = router; 